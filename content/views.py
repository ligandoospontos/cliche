from django.shortcuts import render, get_object_or_404

# Create your views here.
from content.models import Page


def page(request, slug):
    page = get_object_or_404(Page.objects, slug=slug)
    return render(request, 'content/page.html', {'page': page})
