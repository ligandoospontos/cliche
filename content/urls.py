from django.urls import path, include

from content.api import router
from . import views

app_name = 'content'

urlpatterns = [
    path('api/', include(router.urls)),

    path('<slug:slug>/', views.page, name='page'),
]
