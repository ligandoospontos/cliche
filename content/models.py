from django.db import models
from django.utils.html import escape
from markdown import markdown


class Page(models.Model):
    slug = models.SlugField(max_length=100)
    title = models.CharField(max_length=500)

    @property
    def body(self):
        return self.content.html

    def __str__(self):
        return '{} [{}]'.format(self.title, self.slug)


class Content(models.Model):
    page = models.OneToOneField(Page, related_name='content', on_delete=models.CASCADE)

    @property
    def concrete_content(self):
        if hasattr(self, 'htmlcontent'):
            return self.htmlcontent
        elif hasattr(self, 'markdowncontent'):
            return self.markdowncontent
        else:
            return None

    @property
    def html(self):
        return self.concrete_content.html


class HtmlContent(Content):
    content = models.TextField()

    @property
    def html(self):
        return self.content


class MarkdownContent(Content):
    content = models.TextField()

    @property
    def html(self):
        return markdown(escape(self.content))
