from rest_framework import routers, serializers, viewsets, permissions

from content.models import Page


class PageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Page
        fields = ('slug', 'title', 'body')


class PageViewSet(viewsets.ModelViewSet):
    queryset = Page.objects.all()
    permission_classes = (permissions.IsAdminUser,)
    serializer_class = PageSerializer


router = routers.DefaultRouter()
router.register(r'pages', PageViewSet)