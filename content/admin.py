from django import forms
from django.contrib import admin
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _

from content.models import *


class ContentForm(forms.ModelForm):
    concrete_models = {'HTML': HtmlContent, 'Markdown': MarkdownContent}
    content_type = forms.ChoiceField(choices=(
        ('HTML', _("HTML")),
        ('Markdown', _("Markdown"))
    ))
    contents = forms.CharField(widget=forms.Textarea)

    def __init__(self, instance=None, *args, **kwargs):
        super().__init__(instance=instance, *args, **kwargs)
        if instance:
            if hasattr(instance, 'htmlcontent'):
                self.fields['content_type'].initial = 'HTML'
                self.fields['contents'].initial = instance.htmlcontent.content
            elif hasattr(instance, 'markdowncontent'):
                self.fields['content_type'].initial = 'Markdown'
                self.fields['contents'].initial = instance.markdowncontent.content

    def save(self, commit=True):
        content_type_key = self.cleaned_data.get('content_type', None)
        content_type = self.concrete_models[content_type_key]
        contents = self.cleaned_data.get('contents', '')
        if content_type:
            if content_type == HtmlContent:
                content_obj = HtmlContent()
            elif content_type == MarkdownContent:
                content_obj = MarkdownContent()
            else:
                raise ValidationError(_('Invalid content type: {}').format(content_type))
            content_obj.content = contents
            self.instance = content_obj
            return super(ContentForm, self).save(commit=commit)
        else:
            raise ValidationError(_('Invalid content type: {}').format(content_type))

    class Meta:
        model = Content
        fields = ('content_type', 'contents')


class ContentAdmin(admin.StackedInline):
    form = ContentForm
    model = Content


class PageAdmin(admin.ModelAdmin):
    inlines = [ContentAdmin]


admin.site.register(Page, PageAdmin)
