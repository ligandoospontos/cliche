from distutils.core import setup

setup(
    name='cliche',
    version='0.1',
    packages=[''],
    url='https://ligandoospontos.com.br/cliche',
    license='MIT',
    author='André Roque Matheus',
    author_email='amatheus@ligandoospontos.com.br',
    description='Cliche is a CMS developed using django.',
    requires=['markdown', 'django']
)
